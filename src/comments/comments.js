import { useState, useEffect } from "react";
import "./comments.css";
const Comments = (props) => {
  const { commentsData, toggleIsLiked, deleteItem } = props;
  const { id, name, comment, isliked } = commentsData;

  const onClickLikedButton = () => {
    if (isliked) {
      setLikeImageUrl(
        "https://assets.ccbp.in/frontend/react-js/comments-app/like-img.png"
      );
    } else {
      setLikeImageUrl( "https://assets.ccbp.in/frontend/react-js/comments-app/liked-img.png"
        
      );
    }
    toggleIsLiked(id);
  };

  const [likeImageUrl, setLikeImageUrl] = useState("https://assets.ccbp.in/frontend/react-js/comments-app/like-img.png");
  
  
  const onClickDeleteIcon = () => {
    deleteItem(id);
  };

  return (
    <div className="commentsCoontainer">
    <div className="InitialContainer">
    <div className="initalNameContainer">
        {name[0]}
    </div>
      <h1 className="commentBy">{name}</h1>
      </div>
      <p className="comment">{comment}</p>
      <div className="imageContainer">
        <div className="likeContainer">
          <img
            className="likeIcon"
            onClick={() => onClickLikedButton(id)}
            src={likeImageUrl}
          />
          <p className={`liketext ${isliked ? "likedtext":""}`} >Like</p> 
        </div>
        <img
          className="deleteIcon"
          src="https://assets.ccbp.in/frontend/react-js/comments-app/delete-img.png"
          onClick={() => onClickDeleteIcon(id)}
        />
      </div>
      <hr />
    </div>
  );
};
export default Comments;
