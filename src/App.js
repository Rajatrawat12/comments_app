import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import Comments from "./comments/comments.js";
import { v4 as uuidv4 } from "uuid";
function App() {
  const [name, setName] = useState("");
  const [comment, setComment] = useState("");
  const [commentsData, setCommentsData] = useState([]);
  const [commentCount, setCommentCount] = useState(0);
  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleCommentsChange = (event) => {
    setComment(event.target.value);
  };
  const AddCommentButton = () => {
    const newComment = {
      id: uuidv4(),
      name: name,
      comment: comment,
      isliked: false,
    };
    setComment("");
    setName("");
    if ((comment !== "") & (name !== "")) {
      setCommentsData([...commentsData, newComment]);
    } else {
      setCommentsData(commentsData);
    }
    if ((comment !== "") & (name !== "")) {
      setCommentCount(commentCount + 1);
    } else {
      setCommentCount(commentCount);
    }
  };

  const toggleIsLiked = (id) => {
    const newCommentsData = commentsData.map((each) => {
      if (id === each.id) {
        console.log(each);
        return { ...each, isliked: !each.isliked };
      }
      return each;
    });
    setCommentsData(newCommentsData);
  };
  const deleteItem = (id) => {
    const filteredList = commentsData.filter((each) => each.id !== id);
    setCommentCount(commentCount - 1);
    setCommentsData(filteredList);
  };

  return (
    <div className="bgcontainer">
      
      <h1 className="commentsheader">Comments</h1>
      <div className="headercontainer">
        <div>
      <p className="description">Say something about 4.0 Technologies</p>

      <input
        type="text"
        placeholder="Your Name"
        onChange={handleNameChange}
        value={name}
        className="nameInput"
      />
      <br />
      <textarea rows="8" cols="30" name="text" placeholder="Your Comment" onChange={handleCommentsChange} value={comment} className="commentInput"></textarea>
      <br></br>
      <button onClick={AddCommentButton} className="commentAddButton">Add Comment</button>
      </div>
      <img  className="commentsImage"src="https://assets.ccbp.in/frontend/react-js/comments-app/comments-img.png "/>
      </div>
      <hr></hr>
      <p><span className="commentCountCountainer">{commentCount} </span>Comments</p>
      <div>
      
        {commentsData.map((commentData, index) => {
          return (
            <Comments
              key={index}
              commentsData={commentData}
              toggleIsLiked={toggleIsLiked}
              deleteItem={deleteItem}
            />
          );
        })}
      </div>
    </div>
  );
}
export default App;
